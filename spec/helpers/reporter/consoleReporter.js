const JasmineConsoleReporter = require('jasmine-console-reporter');

let consoleReporter = new JasmineConsoleReporter({
    colors: 1,
    cleanStack: 1,
    verbosity: {
      pending: false,
      disabled: false,
      specs: true,
      summary: true
    },
    listStyle: 'indent',
    activity: 'dots',
    emoji: true
});

jasmine.getEnv().addReporter(consoleReporter);