const Jasmine = require('jasmine');

let jasmine = new Jasmine();
// modify this line to point to your jasmine.json
jasmine.loadConfigFile('./config/jasmine.json');
jasmine.execute();