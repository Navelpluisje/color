const { Color } = require('../dist/umd');

describe("The hsl(a) value", () => {
  const color = new Color('hsla(0, 100%, 50%, .5)');
  const color2 = new Color('hsl(0, 0%, 50%)');

  it("should convert to the correct rgb and alpha channels", () => {
    expect(color.red).toBe(1);
    expect(color.green).toBe(0);
    expect(color.blue).toBe(0);
    expect(color.alpha).toBe(0.5);
  });

  it("should convert to the correct rgb channels", () => {
    expect(color2.red).toBe(0.5);
    expect(color2.green).toBe(0.5);
    expect(color2.blue).toBe(0.5);
    expect(color2.alpha).toBe(1);
  });

  it("should return a valid hsl(a) value", () => {
    expect(color.getHsl()).toBe('hsla(0, 100%, 50%, 0.5)');
    expect(color2.getHsl()).toBe('hsl(0, 0%, 50%)');
  });
});

describe("The hex(a) value", () => {
  const color = new Color('#00ff0080');
  const color2 = new Color('#808080');

  it("should convert to the correct rgb and alpha channels", () => {
    expect(color.red).toBe(0);
    expect(color.green).toBe(1);
    expect(color.blue).toBe(0);
    expect(color.alpha.toPrecision(2)).toBe('0.50');
  });

  it("should convert to the correct rgb channels", () => {
    expect(color2.red.toPrecision(2)).toBe('0.50');
    expect(color2.green.toPrecision(2)).toBe('0.50');
    expect(color2.blue.toPrecision(2)).toBe('0.50');
    expect(color2.alpha).toBe(1);
  });

  it("should return a valid hsl(a) value", () => {
    expect(color.getHsl()).toBe('hsla(120, 100%, 50%, 0.5)');
    expect(color2.getHsl()).toBe('hsl(0, 0%, 50%)');
  });
});

describe("The rgb(a) value", () => {
  const color = new Color('rgba(0, 0, 255, .5)');
  const color2 = new Color('rgb(128, 128, 128)');

  it("should convert to the correct rgb and alpha channels", () => {
    expect(color.red).toBe(0);
    expect(color.green).toBe(0);
    expect(color.blue).toBe(1);
    expect(color.alpha.toPrecision(2)).toBe('0.50');
  });

  it("should convert to the correct rgb channels", () => {
    expect(color2.red.toPrecision(2)).toBe('0.50');
    expect(color2.green.toPrecision(2)).toBe('0.50');
    expect(color2.blue.toPrecision(2)).toBe('0.50');
    expect(color2.alpha).toBe(1);
  });

  it("should return a valid hsl(a) value", () => {
    expect(color.getHsl()).toBe('hsla(240, 100%, 50%, 0.5)');
    expect(color2.getHsl()).toBe('hsl(0, 0%, 50%)');
  });
});

