const REGEX_RGB = /rgb[a]?\(\s*[0-9]{1,3},\s*[0-9]{1,3},\s*[0-9]{1,3}(,\s*(0)?\.[0-9]{1,2})?\s*\)/i;
const REGEX_HSL = /hsl[a]?\(\s*([0-9]{1,2}|[1-2][0-9]{2}|[3][0-5][0-9]|360),\s*[0-9]{1,3}%,\s*[0-9]{1,3}%(,\s*(0)?\.[0-9]{1,2})?\s*\)/i;
const REGEX_HEX = /(^(#?[0-9a-fA-F]{2}){3,4}|^(#?[0-9a-fA-F]){3})$/;
const REGEX_NAME = /^[a-z]{2,}$/i;

export class Color {
  constructor(color) {
    this.red = 0;
    this.green = 0;
    this.blue = 0;
    this.alpha = 1;

    this.getRgbValues(color);
  }

  /**
  * Check which type of color we have and call it's corresponding converter
  * If none of the types matches black will be returned
  * @param {string} color
  * @returns {object} Object containing the rgb values from 0 - 1
  */
  getRgbValues(color) {
    if (REGEX_RGB.test(color)) {
      this.rgbToRGB(color);
    } else if (REGEX_HSL.test(color)) {
      this.hslToRGB(color);
    } else if (REGEX_HEX.test(color)) {
      this.hexToRGB(color);
    } else if (REGEX_NAME.test(color)) {
      this.nameToRGB(color);
    } else {
      console.error('No valid color provided');
    }
  }

  /**
  * Convert a rgb color string into an array with the rgb elements
  * @param {string} rgb RGB string to convert to an array
  * @return {{red: number, green: number, blue: number}}
  */
  rgbToRGB(rgb) {
    /**
    * Create an array of the returned string and remove all the 'clutter'
    * Then split it by the comma to get an array.
    * Loop through the array and replace all the elements by it's value divided by 255
    */
    const arrRGB = rgb
      .replace(/([rgb|rgba]|\(|\s|\))/g, '')
      .split(',')
      .map(a => parseFloat(a));

    this.red = arrRGB[0] / 255;
    this.green = arrRGB[1] / 255;
    this.blue = arrRGB[2] / 255;
    this.alpha = arrRGB[3] || 1;
  }


  /**
  * Convert a rgb hexadecimal string to an array with the rgb elements
  * @param {string} hex RGB string to convert to an array
  * @returns {object} An object representing the color, [0,0,0] as fallback
  */
  hexToRGB(_hex = '#000') {
    let hex = _hex;
    /**
    * If there is a hash we wil remove it
    */
    if (hex.includes('#')) {
      hex = hex.replace('#', '');
    }

    /**
    * If we have a shortly written hex value, we need to convert it to the regular length
    */
    if (hex.length === 3 || hex.length === 4) {
      hex = hex.split('')
        .map(a => `${a}${a}`)
        .join('');
    }

    /**
    * First we parse the int value of the hex code.
    * Then divide by 255 to create a percentage representation.
    */
    this.red = parseInt(hex.substr(0, 2), 16) / 255;
    this.green = parseInt(hex.substr(2, 2), 16) / 255;
    this.blue = parseInt(hex.substr(4, 2), 16) / 255;
    this.alpha = +(parseInt(hex.substr(6, 2), 16) / 255 || 1).toPrecision(2);
  }

  hslToRGB(hsl) {
    /**
    * Create an array of the returned string and remove all the 'clutter'
    * Then split it by the comma to get an array.
    * Loop through the array and replace all the elements by it's value divided by 255
    */
    const arrHSL = hsl
      .replace(/([hsl|hsla]|\(|\s|\)|%)/g, '')
      .split(',')
      .map(a => parseFloat(a));

    const hue = arrHSL[0] / 360;
    const saturation = arrHSL[1] / 100;
    const lightness = arrHSL[2] / 100;
    this.alpha = arrHSL[3] || 1;

    if (saturation === 0) {
      this.red = lightness;
      this.green = lightness;
      this.blue = lightness; // achromatic
    } else {
      const hue2rgb = (p, chroma, _t) => {
        let t = _t;
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (chroma - p) * 6 * t;
        if (t < 1 / 2) return chroma;
        if (t < 2 / 3) return p + (chroma - p) * (2 / 3 - t) * 6;
        return p;
      };

      const chroma = lightness < 0.5
        ? lightness * (1 + saturation)
        : lightness + saturation - lightness * saturation;
      const p = 2 * lightness - chroma;
      this.red = hue2rgb(p, chroma, hue + 1 / 3);
      this.green = hue2rgb(p, chroma, hue);
      this.blue = hue2rgb(p, chroma, hue - 1 / 3);
    }
  }

  /**
  * Convert a color by it's name to an array with the rgb elements
  * @param {string} name The color name to convert
  * @returns {object} An object representing the color, [0,0,0] as fallback
  */
  nameToRGB(name) {
    /**
    * Create a dom-element to give the given color
    * and the body to append it to and delete it from.
    */
    const dom = document.createElement('span');
    const body = document.querySelector('body');

    /**
    * Set the element color style attribute to the given color
    * and append it now to the body.
    * It needs to be added to the body, otgherwise we are not able to call window.getComputedStyle()
    */
    dom.style.color = name;
    body.appendChild(dom);

    /**
    * Extract the rgb color from the dom element. This will return a rgb(xx, xx, xx) string
    * Now remove the temporary element because we do not want to obstruct anything in the dom
    */
    const domRGB = window.getComputedStyle(dom).color;
    body.removeChild(dom);

    this.rgbToRGB(domRGB);
  }

  /**
  * Create HSL color notation by rgb values.
  * Calculation from: https://nl.wikipedia.org/wiki/HSL_(kleurruimte)
  * @param {string} color Any CSS color style description and HSV
  * @returns {object} Object with the HSL values
  */
  getHsl() {
    /**
    * First we get the rgb colors and get the minimum and maximum value of those values
    */
    const min = Math.min(this.red, this.green, this.blue);
    const max = Math.max(this.red, this.green, this.blue);
    const maxDelta = max - min;
    /**
    * First one to really set is the lightness
    * Looks like the easiest. The lower the values, the darker it gets
    */
    const lightness = (min + max) / 2;
    let saturation = 0;
    let hue = 0;

    /**
    * If the minimum equals the maximum, the other is also the same.
    * So we have a gray color, so only lightness is relevant
    */
    if (min === max) {
      return this.createHslOutput(hue, saturation, lightness);
    }

    /**
    * Set the saturation by the value of lightness.
    * This because of the double cone structure of HSL,
    */
    if (lightness < 0.5) {
      saturation = maxDelta / (max + min);
    } else {
      saturation = maxDelta / (2 - (max + min));
    }

    /**
    * Finally set the hue. First create the value and then
    * turn it into degrees by multiplying it with 60
    */
    if (this.red === max) {
      hue = ((this.green - this.blue) / maxDelta);
      /**
      * If the value is negative we want to turn it into a positive value.
      * By adding 6, it will be positive again and with the right degrees
      */
      if (hue < 0) {
        hue += 6;
      }
    } else if (this.green === max) {
      /**
      * Green is at 120 degrees. Beside this we will add or subtract the amount of offset
      */
      hue = 2 + ((this.blue - this.red) / maxDelta);
    } else if (this.blue === max) {
      /**
      * Blue is at 240 degrees. Beside this we will add or subtract the amount of offset
      */
      hue = 4 + ((this.red - this.green) / maxDelta);
    } else {
      /**
      * Something really weird happened. return a gray color with the calculated lightness
      */
      return this.createHslOutput(hue, saturation, lightness);
    }
    /**
    * To set the hue to the right amount of degrees we need to multiply it by 60
    */
    hue *= 60;

    return this.createHslOutput(hue, saturation, lightness);
  }

  createHslOutput(hue, saturation, lightness) {
    const sat = parseInt(saturation * 100, 10);
    const light = parseInt(lightness * 100, 10);

    if (this.alpha !== 1) {
      return `hsla(${hue}, ${sat}%, ${light}%, ${this.alpha})`;
    }
    return `hsl(${hue}, ${sat}%, ${light}%)`;
  }
}

export default null;
