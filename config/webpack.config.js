const path = require('path');
const commonConfig = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          configFile: 'config/.eslintrc.json',
          ignorePath: 'config/.lintignore',
        },
      },
    ],
  },
}

const umdConfig = {
  entry: './src/color.js',
  output: {
    filename: 'umd.js',
    path: path.resolve(__dirname, '../dist'),
    library: 'npComponents',
    libraryTarget: 'umd',
  },
  mode: 'production',
  target: 'node',
};

const cjsConfig = {
  entry: './src/color.js',
  output: {
    filename: 'commonjs.js',
    path: path.resolve(__dirname, '../dist'),
    library: 'npComponents',
    libraryTarget: 'commonjs',
  },
  mode: 'production',
};

module.exports = [
  Object.assign({}, commonConfig, umdConfig),
  Object.assign({}, commonConfig, cjsConfig),
];
