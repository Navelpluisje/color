# Color

[![npm (scoped)](https://img.shields.io/npm/v/@navelpluisje/color.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/color)
[![npm (scoped)](https://img.shields.io/npm/dy/@navelpluisje/color.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/color)
[![npm (scoped)](https://img.shields.io/npm/l/@navelpluisje/color.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/color)
[![npm (scoped)](https://img.shields.io/bitbucket/issues-raw/navelpluisje/color.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/color)
[![npm (scoped)](https://img.shields.io/badge/made%20with-%E2%9D%A4-red.svg?style=flat-square)](https://navelpluisje.nl)

## About

This is a small library for creating hsl(a) color values. The input can be either a hex, rgb(a), hsl(a) or named color.

## Usage

### install
```
  npm i -S @navelpluisje/color
```

### ES6 import

### Javascript

```javascript
import { Color } from '@navelpluisje/color/src';

const color = new Color('#f00');
concole.log(color.getHsl());
// Will output: hsl(0, 100%, 50%)
```

### UMD

```javascript
const { Color } = require('@navelpluisje/color/dist/umd');

const color = new Color('#0f0');
concole.log(color.getHsl());
// Will output: hsl(120, 100%, 50%)
```

## Issues

If you found a bug, room for improvement or whatever, you can [create an issue](https://bitbucket.org/Navelpluisje/pcb-components/issues?status=new&status=open) here.

Please check first if your issue already hes been reported.

## Requests

If you have any idea for more usefull component (needs to be in pcb style) you can [create an issue](https://bitbucket.org/Navelpluisje/pcb-components/issues/new) with a request.


## Contribute

Just clone the repository, add your feature/imrovement an create a PR.
When contributing please respect the linting and add ests for the changes you made.
